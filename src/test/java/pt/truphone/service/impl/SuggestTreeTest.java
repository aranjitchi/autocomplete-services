package pt.truphone.service.impl;

import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import pt.truphone.App;
import pt.truphone.service.AutoComplete;
import pt.truphone.service.AutoCompleteResult;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@Import(App.class)
public class SuggestTreeTest {

    @Autowired
    private AutoComplete suggestTree;

    /*
    @Autowired()
    @Qualifier(value = "ds")
    List<String> values;

    @Before
    public void init() {
        //values.forEach(value -> suggestTree.add(value.toUpperCase()));
    }
     */
    @Test
    public void autocomplete_Give2ResultsForWordsAndChars_True() {
        AutoCompleteResult input1 = suggestTree.autocomplete("dart");

        List<String> words = input1.getWords();
        List<Character> chars = input1.getChars();

        assertThat(words).contains("DARTFORD", "DARTMOUTH");
        assertThat(chars).contains('F', 'M');
        assertThat(words.size()).isEqualTo(2);
        assertThat(chars.size()).isEqualTo(2);
    }

    @Test
    public void autocomplete_Give2ResultsForWordsAnd1ForChar_True() {
        //Arrange
        AutoCompleteResult input2 = suggestTree.autocomplete("LIVERPOOL");

        //Act
        List<String> words = input2.getWords();
        List<Character> chars = input2.getChars();

        //Assert
        assertThat(words).contains("LIVERPOOL", "LIVERPOOL LIME STREET");
        assertThat(chars).contains(' ');
        assertThat(words.size()).isEqualTo(2);
        assertThat(chars.size()).isEqualTo(1);
    }

    @Test
    public void autocomplete_WithoutResults_True() {
        AutoCompleteResult input3 = suggestTree.autocomplete("KINGS CROSS");

        List<String> words = input3.getWords();
        List<Character> chars = input3.getChars();

        assertThat(words.size()).isEqualTo(0);
        assertThat(chars.size()).isEqualTo(0);
    }

    @Test
    public void autocomplete_UsingAccentsAndNoResults_True() {
        AutoCompleteResult input4 = suggestTree.autocomplete("António");

        List<String> words = input4.getWords();
        List<Character> chars = input4.getChars();

        assertThat(words.size()).isEqualTo(0);
        assertThat(chars.size()).isEqualTo(0);
    }

    @Test
    public void autocomplete_Give1ResultForWordAndChar_True() {
        AutoCompleteResult input5 = suggestTree.autocomplete("odivela");

        List<String> words = input5.getWords();
        List<Character> chars = input5.getChars();

        assertThat(words).contains("ODIVELAS");
        assertThat(chars).contains('S');
        assertThat(words.size()).isEqualTo(1);
        assertThat(chars.size()).isEqualTo(1);
    }

    @Test
    public void autocomplete_Give1ResultForWordAndCharWhenLastInputCharIsSpace_True() {
        AutoCompleteResult input6 = suggestTree.autocomplete("senhor ");

        List<String> words = input6.getWords();
        List<Character> chars = input6.getChars();

        assertThat(words).contains("SENHOR ROUBADO");
        assertThat(chars).contains('R');
        assertThat(words.size()).isEqualTo(1);
        assertThat(chars.size()).isEqualTo(1);
    }

    @Test
    public void add_GiveNewWord_True() {
        suggestTree.add("dartamit");
    }

    @Test
    public void autocomplete_InsertedWordValidation_True() {
        AutoCompleteResult input7 = suggestTree.autocomplete("dart");

        List<String> words = input7.getWords();
        List<Character> chars = input7.getChars();

        assertThat(words).contains("DARTFORD", "DARTMOUTH", "DARTAMIT");
        assertThat(chars).contains('F', 'M', 'A');
        assertThat(words.size()).isEqualTo(3);
        assertThat(chars.size()).isEqualTo(3);
    }

    @Test
    public void autocomplete_CharListMustGiveNonDuplicatedValues_True() {
        AutoCompleteResult input8 = suggestTree.autocomplete("o");

        List<String> words = input8.getWords();
        List<Character> chars = input8.getChars();

        assertThat(words).contains("ODIVELAS", "OLAIAS", "OLIVAIS", "ORIENTE");
        assertThat(chars).contains('D', 'L', 'R');
        assertThat(words.size()).isEqualTo(4);
        assertThat(chars.size()).isEqualTo(3);
    }

}
