package pt.truphone.service;

import java.io.Serializable;

/*
 * Interface que permite implementar o algoritmo do autocomplete
 */
public interface AutoComplete extends Serializable {

    /**
     * Carrega os valores para:
     * <ul>
     * <li>lista</li>
     * <li>mapa</li>
     * <li>ficheiro</li>
     * <li>base de dados</li>
     * <li>etc...</li>
     * </ul>
     *
     * @param value valor
     */
    void add(String value);

    /**
     * Devolve os resultados
     *
     * @param prefix palavra a ser procurada
     * @return devolve as palavras como a letra seguinte ao prefixo
     * @see AutoCompleteResult
     */
    AutoCompleteResult autocomplete(String prefix);

}
