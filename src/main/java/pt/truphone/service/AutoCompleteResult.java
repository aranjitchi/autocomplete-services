package pt.truphone.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Devolve a lista de resultados (inclui: valor(es) e proxima(s) letra(s))
 */
public class AutoCompleteResult implements Serializable {

    private String prefix;
    final private List<String> words;
    private List<Character> chars;

    public AutoCompleteResult() {
        this.words = new ArrayList<>();
        this.chars = new ArrayList<>();
    }

    public AutoCompleteResult(List<String> words, String prefix) {
        this.words = words;
        this.prefix = prefix;
        this.chars = new ArrayList<>();
    }

    public List<String> getWords() {
        return words;
    }

    public List<Character> getChars() {
        words.stream().forEach(word -> {
            if (prefix.length() < word.length()) {
                if (!chars.contains(word.charAt(prefix.length()))) {
                    chars.add(word.charAt(prefix.length()));
                }
            }
        });
        return chars;
    }

}
