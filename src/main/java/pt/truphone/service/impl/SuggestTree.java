package pt.truphone.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pt.truphone.aspect.Audit;
import pt.truphone.service.AutoComplete;
import pt.truphone.service.AutoCompleteResult;

/**
 * Algoritmo baseado no mecanismo em arvore
 */
@Service
public class SuggestTree implements AutoComplete {

    TrieNode trieNode = null;


    /* Preencher previamente com dados */
    @Autowired
    @Qualifier(value = "ds")
    List<String> values;

    @PostConstruct
    public void init() {
        this.trieNode = new TrieNode();
        values.forEach(value -> add(value.toUpperCase()));
    }

    @Override
    public void add(String value) {
        if (this.trieNode == null) {
            this.trieNode = new TrieNode();
        };
        trieNode.insert(value.toUpperCase());
    }

    @Override
    @Audit
    public AutoCompleteResult autocomplete(String prefix) {
        String prefixUpper = prefix.toUpperCase();
        TrieNode actualNode = trieNode;
        for (char c : prefixUpper.toCharArray()) {
            if (!actualNode.children.containsKey(c)) {
                return new AutoCompleteResult(Collections.emptyList(), prefixUpper);
            }
            actualNode = actualNode.children.get(c);
        }
        return new AutoCompleteResult(actualNode.words, prefixUpper);
    }
}

class TrieNode implements Serializable {

    final Map<Character, TrieNode> children;
    List<String> words;

    public TrieNode() {
        this.children = new HashMap<>();
        this.words = new ArrayList<>();
    }

    public TrieNode(String word) {
        this();
        this.words.add(word);
    }

    public void insert(String word) {
        if (word == null) {
            throw new NullPointerException("Cannot add null value to a TrieNode");
        }
        TrieNode node = this;
        for (char c : word.toCharArray()) {
            if (!node.children.containsKey(c)) {
                node.children.put(c, new TrieNode(word));
            }
            node = node.children.get(c);
            if (!node.words.contains(word)) {
                node.words.add(word);
            }
        }
    }
}
