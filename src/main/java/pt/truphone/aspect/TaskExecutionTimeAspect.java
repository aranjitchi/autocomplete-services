package pt.truphone.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TaskExecutionTimeAspect {

    final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Around("@annotation(pt.truphone.aspect.Audit)")
    public Object getTimeInMilis(ProceedingJoinPoint pjp) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object retVal = pjp.proceed();
        long endTime = System.currentTimeMillis();
        LOGGER.info(String.format("%s took %s milliseconds", pjp.getSignature().toString(), (endTime - startTime)));
        return retVal;
    }
}
