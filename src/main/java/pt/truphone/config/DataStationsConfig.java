package pt.truphone.config;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataStationsConfig {

    @Bean(name = "ds")
    public List<String> ds() {
        List<String> ds = new ArrayList<>();
        ds.add("DARTFORD");
        ds.add("DARTMOUTH");
        ds.add("TOWER HILL");
        ds.add("DERBY");
        ds.add("LIVERPOOL");
        ds.add("LIVERPOOL LIME STREET");
        ds.add("PADDINGTON");
        ds.add("EUSTON");
        ds.add("LONDON BRIDGE");
        ds.add("VICTORIA");
        ds.addAll(dsPt());
        return ds;
    }

    @Bean(name = "ds1")
    public List<String> ds1() {
        List<String> ds1 = new ArrayList<>();
        ds1.add("DARTFORD");
        ds1.add("DARTMOUTH");
        ds1.add("TOWER HILL");
        ds1.add("DERBY");
        return ds1;
    }

    @Bean(name = "ds2")
    public List<String> ds2() {
        List<String> ds2 = new ArrayList<>();
        ds2.add("LIVERPOOL");
        ds2.add("LIVERPOOL LIME STREET");
        ds2.add("PADDINGTON");
        return ds2;
    }

    @Bean(name = "dsPt")
    public List<String> dsPt() {
        List<String> dsPt = new ArrayList<>();
        dsPt.add("Aeroporto");
        dsPt.add("Alameda");
        dsPt.add("Alfornelos");
        dsPt.add("Alto dos Moinhos");
        dsPt.add("Alvalade");
        dsPt.add("Amadora Este");
        dsPt.add("Ameixoeira");
        dsPt.add("Anjos");
        dsPt.add("Areeiro");
        dsPt.add("Arroios");
        dsPt.add("Avenida");
        dsPt.add("Baixa-Chiado");
        dsPt.add("Bela Vista");
        dsPt.add("Cabo Ruivo");
        dsPt.add("Cais do Sodré");
        dsPt.add("Campo Grande");
        dsPt.add("Carnide");
        dsPt.add("Chelas");
        dsPt.add("Cidade Universitária");
        dsPt.add("Colégio Militar");
        dsPt.add("Encarnação");
        dsPt.add("Intendente");
        dsPt.add("Jardim Zoológico");
        dsPt.add("Laranjeiras");
        dsPt.add("Lumiar");
        dsPt.add("Martim Moniz");
        dsPt.add("Moscavide");
        dsPt.add("Odivelas");
        dsPt.add("Olaias");
        dsPt.add("Olivais");
        dsPt.add("Oriente");
        dsPt.add("Parque");
        dsPt.add("Picoas");
        dsPt.add("Pontinha");
        dsPt.add("Praça de Espanha");
        dsPt.add("Quinta das Conchas");
        dsPt.add("Rato");
        dsPt.add("Reboleira");
        dsPt.add("Restauradores");
        dsPt.add("Roma");
        dsPt.add("Rossio");
        dsPt.add("Saldanha");
        dsPt.add("Santa Apolónia");
        dsPt.add("São Sebastião");
        dsPt.add("Senhor Roubado");
        dsPt.add("Telheiras");
        dsPt.add("Terreiro do Paço");
        return dsPt;
    }

}
