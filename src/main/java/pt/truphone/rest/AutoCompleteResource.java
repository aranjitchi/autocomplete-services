package pt.truphone.rest;

import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.truphone.service.AutoComplete;
import pt.truphone.service.AutoCompleteResult;

@RestController
@RequestMapping("/v1/train/stations")
public class AutoCompleteResource {

    @Autowired
    AutoComplete suggestTree;

    @PostMapping(value = "/add", produces = MediaType.TEXT_PLAIN_VALUE)
    @Async
    public CompletableFuture<String> addNewWord(@RequestBody String word) {
        String modifiedWord = word.toUpperCase();
        suggestTree.add(modifiedWord);
        return CompletableFuture.completedFuture(modifiedWord);
    }

    @GetMapping
    public AutoCompleteResult getAutoCompletedResults(@RequestParam(value = "search") String prefix) {
        return suggestTree.autocomplete(prefix);
    }
}
